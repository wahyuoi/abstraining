Berikut adalah penjelasan dari beberapa file yang ada di repository ini

- Modul-modul untuk model(class):
  1. Modul Program yang bisa anda temukan di Program.abs
  2. Modul Publication yang bisa anda temukan di Publication.abs
  3. Modul ObjectiveTarget yang bisa anda temukan di ObjectiveTarget.abs
  4. Modul FinanceInventory yang bisa anda temukan di FinanceInventory.abs

- Modul Feature Model barada di COS_FeatureModel.abs

- Modul Product Configuration dapat anda temukan di COSPL.abs

- Modul Product Selection dapat ditemukan Product.abs

- Untuk main block berada di modul COS, yaitu pada COS.abs


--- Potongan text dari pengembangan AISCO sebagai gambaran rencana kerja latihan dan diskusi

AISCO: Adaptive Information Systems for Charity Organizations

Core product of AISCO should at least provide:

(a) information about activity of the organization:
    which include:
    name, PIC, Initial activity, contact information,
    Brief description. 

(b) Information of who, (user list), when; edit history is needed.

(c) finance report can be uploaded or generated.
    It indicated the need of finance report, which the data
    can be taken from a specific uploaded documents
    or generated from the income and expense feature.

(d) the income should contain: accountcode, amount, donor,
    means (bank transfer, or cash, item), confirmation status,
    softcopy of the proof, type of donation (general or member)

(e) the expense should contain: accountcode, amount, Person in Charge

